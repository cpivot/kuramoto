#include <feel/feel.hpp>
#include <feel/feelmodels/modelproperties.hpp>

#include <armadillo>

inline
Feel::po::options_description
makeOptions()
{
    Feel::po::options_description kuramotooptions( "kuramoto problem options" );
    kuramotooptions.add_options()
        ( "linearization", Feel::po::value<bool>()->default_value( true ), "u^{n+1} grad u^{n}" )
        ;
    return kuramotooptions.add( Feel::feel_options() );
}

int main(int argc, char**argv )
{
    using namespace Feel;

    Environment env( _argc=argc, _argv=argv,
                     _desc=makeOptions(),
                     _about=about(_name="kuramoto", // That help us loading kuramoto.cfg
                                  _author="Charles Pivot",
                                  _email="pivot.cp@gmail.com"));

    auto mesh = loadMesh(_mesh = new Mesh<Simplex<1>>);

    auto Vh = Pch<2>( mesh );
    auto u = Vh->element();
    auto v = Vh->element();

    // --mod-file is loadded here
    ModelProperties model;

    u.on(_range=elements(mesh),_expr=expr(soption("functions.u")));

    auto M_bdf = bdf( _space=Vh );
    M_bdf->initialize(u);
    auto l = form1( _test=Vh );
    auto a  = form2( _trial=Vh, _test=Vh);
    auto as = form2( _trial=Vh, _test=Vh);

    arma::mat result;
    int arma_size = (int)(doption("parameters.a"));
    if ( Environment::isMasterRank() )
        result=arma::zeros(arma_size,(int)(M_bdf->timeFinal()/M_bdf->timeStep())+1);

    int ii=0;

    auto Jacobian = [&](const vector_ptrtype& X, sparse_matrix_ptrtype& J)
        {
            if (!J) J = backend()->newMatrix( Vh, Vh );
            auto a = form2( _test=Vh, _trial=Vh, _matrix=J );
            a.zero();
            for(auto mat : model.materials())
            {
              LOG(INFO) << "Dealing with " << mat.first << "\n";
              a+=integrate(_range=markedelements(mesh,mat.first),
                          _expr=M_bdf->polyDerivCoefficient(0)*inner(idt(u),id(v))
                          + mat.second.getDouble("coef_laplacien")*inner(laplaciant(u),laplacian(u))
                          - mat.second.getDouble("coef_grad")*inner(gradt(u),grad(u))
                          + mat.second.getDouble("coef_NL")*inner(idt(u)*gradv(u)+idv(u)*gradt(u),id(v)));
            }
            for(auto bc : model.boundaryConditions().getScalarFields<2>("u","Dirichlet"))
            {
              LOG(INFO) << "Applying BC " << bc.second << " on " << bc.first << "\n";
              a+=on(_range=markedpoints(mesh,bc.first),
                 _element=u,
                 _expr=bc.second,
                 _rhs=l);
            }
        };

    auto Residual = [&](const vector_ptrtype& X, vector_ptrtype& R)
        {
            LOG(INFO) << "Residual\n";
            auto u = Vh->element();
            u = *X;
            if (!R) R = backend()->newVector( Vh );
            auto r = form1( _test=Vh, _vector=R );

            r.zero();
            for(auto mat : model.materials())
            {
              LOG(INFO) << "Dealing with " << mat.first << "\n";
              r += integrate(_range=markedelements(mesh,mat.first),
                          _expr=(
                                inner(M_bdf->polyDerivCoefficient(0)*idv(u)-idv(M_bdf->polyDeriv()),id(v))
                              + mat.second.getDouble("coef_laplacien")*inner(laplacianv(u),laplacian(v))
                              - mat.second.getDouble("coef_grad")*inner(gradv(u),grad(u))
                              + mat.second.getDouble("coef_NL")*inner(idv(u)*gradv(u),id(v))));
            }
            LOG(INFO) << "End of residual\n";
        };

    auto addToMat = [&] ()
    {
        for (int jj=0;jj<arma_size;jj++)
        {
            node_type aa(1);
            aa(0)=(1./arma_size)*static_cast<double>(jj);

            // This operation has to be done by every process !
            double toto=u(aa)(0,0,0);
            if ( Environment::isMasterRank() )
                result(jj,ii)=toto;
        }
        ii++;
    };

    backend()->nlSolver()->residual = Residual;
    backend()->nlSolver()->jacobian = Jacobian;

    if(boption("linearization"))
    {
      as.zero();
      for(auto mat : model.materials())
      {
        LOG(INFO) << "Dealing with " << mat.first << "\n";
        as += integrate(_range=markedelements(mesh,mat.first),
            _expr=M_bdf->polyDerivCoefficient(0)*inner(idt(u),id(v)));
        as += integrate(_range=markedelements(mesh,mat.first),
            _expr=+mat.second.getDouble("coef_laplacien")*inner(laplaciant(u),laplacian(v)));
        as += integrate(_range=markedelements(mesh,mat.first),
            _expr=-mat.second.getDouble("coef_grad")*inner(gradt(u),grad(v)));
      }
    }
    addToMat();

    for ( M_bdf->start(); M_bdf->isFinished()==false;M_bdf->next(u) )
    {
        Feel::cout << M_bdf->time() << std::endl;

        if(boption("linearization"))
        {
            a=as;
            l.zero();
            for(auto mat : model.materials())
            {
              LOG(INFO) << "Dealing with " << mat.first << "\n";
              a += integrate(_range=markedelements(mesh,mat.first),
                  _expr=+mat.second.getDouble("coef_NL")*inner(idt(u)*gradv(u),id(v) ));
              l += integrate(_range=markedelements(mesh,mat.first),
                  _expr=+inner(idv(M_bdf->polyDeriv()),id(v)) );
            }
            for(auto bc : model.boundaryConditions().getScalarFields<2>("u","Dirichlet"))
            {
              LOG(INFO) << "Applying BC " << bc.second << " on " << bc.first << "\n";
              a+=on(_range=markedpoints(mesh,bc.first), _rhs=l, _element=u, _expr=bc.second );
            }

          a.solve(_rhs=l,_solution=u);
        }
        else
        {
          for(auto bc : model.boundaryConditions().getScalarFields<2>("u","Dirichlet"))
            u = vf::project(_space=Vh,_range=markedpoints(mesh,bc.first),_expr=bc.second);
          backend()->nlSolve( _solution=u );
        }

        addToMat();
    }

    if ( Environment::isMasterRank() )
        result.save("result.dat",arma::raw_ascii);

    return 0;
}
