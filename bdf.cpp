#include <feel/feel.hpp>

/*
 * Only solve d_t u = 0; u(t=0) = u_0
 * written to test the bdf order
 */
int main(int argc, char **argv)
{
    using namespace Feel;

    Environment env( _argc=argc, _argv=argv,
                     _about=about(_name="bdf_bug", // That help us loading kuramoto.cfg
                                  _author="Vincent HUBER",
                                  _email="vincent.huber@gmail.com"));

    auto mesh = loadMesh(_mesh = new Mesh<Simplex<1>>);

    auto Vh = Pch<ORDER>( mesh );
    auto u = Vh->element();
    auto v = Vh->element();
    u.on(_range=elements(mesh),_expr=expr(soption("functions.u")));
    auto M_bdf = bdf( _space=Vh );

    auto a = form2( Vh, Vh);
    auto l = form1( Vh );

    auto e = exporter(_mesh = mesh );

    for ( M_bdf->start(u); M_bdf->isFinished()==false;M_bdf->next(u) )
    {
      a = integrate(_range=elements(mesh), _expr=M_bdf->polyDerivCoefficient(0)*inner(idt(u), id(v)));
      l = integrate(_range=elements(mesh), _expr=inner(idv(M_bdf->polyDeriv()), id(v)));
      a.solve(_rhs=l, _solution=u);
      double n = integrate( _range= elements( mesh ), _expr= norm2(idv(u)) ).evaluate()( 0,0 );
      Feel::cout << "n2 = " << n << std::endl;
      e->step(M_bdf->time())->add("u",u);
      e->step(M_bdf->time())->add("u_old",M_bdf->polyDeriv());
      e->save();
    }


  return 0;
}
