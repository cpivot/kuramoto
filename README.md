The maths are [here](http://vhuber.aerobatic.io/2016/09/25/kuramoto-sivashinsky)

Varying the snes-type can help (ls, tr, ksponly ...) to debug.
to show result.dat

```sh
set cbrange [-1:1]
plot "results.dat" matrix w image
```
