// -*- mode: javascript -*- vim: set ft=javascript:
{
  "Name": "GENERIC ",
  "ShortName":"generic",
  "Model":"No idea",
  "Materials":
  {
    "Mat1":{
      "name":"Mat1",
      "coef_laplacien":"2e-3",
      "coef_grad":"1",
      "coef_NL":"1",
      "param4":"1" 
    }
  },
  "Parameters":
  {
  },
  "BoundaryConditions":
  {
    "u": // Var name 
    {
      "Dirichlet": // cond type
      {
        "ptA": // marker name
        {
          "expr":"0"
        },
        "ptB": // marker name
        {
          "expr":"0"
        }
      }
    }
  }
}
