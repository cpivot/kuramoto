h = 0.001;
x1 =   0;
x2 = 100;
p1 = newp; Point(p1) = {x1,0,0,h};
p2 = newp; Point(p2) = {x2,0,0,h};
l1 = newl; Line(l1) = {p1,p2};
Physical Point("ptA") = {p1};
Physical Point("ptB") = {p2};
Physical Line("Mat1") = {l1};
