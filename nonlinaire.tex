\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{bm}
\usepackage{mathenv}
\usepackage[]{algorithm2e}


\title{Résolution via une méthode de Newton d'un problème non linéaire}
\author{Charles Pivot}
\date{}


\begin{document}
\maketitle
On considère le problème suivant : 
\begin{equation}
  \left\{
    \begin{aligned}
      & & \partial_t \bm{u} + A(\bm{u}) = \bm{F} ,\quad \text{dans } \Omega\\
      & & \bm{u}=0 ,\quad \text{sur } \partial\Omega\\
    \end{aligned}
  \right.\label{eq:problem}
\end{equation}

\section{Backward Euler Method}
A l'ordre 1, on peut écrire 
\begin{equation}
\partial_t U \approx \dfrac{U^{n+1}}{\delta t}-\dfrac{U^{n}}{\delta t}
\end{equation}
Et de manière plus général à l'ordre $k \in \mathbb{N}$ : 
\begin{equation}
\partial_t U \approx \dfrac{U^{n+1}}{\delta t}-f(U^{n},U^{n-1},\cdots,U^{n-k})
\end{equation}
Ainsi le problème \ref{eq:problem} peut s'écrire : 
\begin{equation}
\dfrac{\bm{u}^{n+1}}{\delta t} + A(\bm{u}^{n+1}) =  \bm{F}+f(\bm{u}^{n},\bm{u}^{n-1},\cdots,\bm{u}^{n-k}) \label{eq:problem_bdf}
\end{equation}

\section{Formulation faible}
La forme variationnelle du problème \ref{eq:problem} revient à trouver $\bm{u} \in H^1_0 (\Omega) $, avec $\left\langle \cdot , \cdot \right\rangle$ un produit scalaire sur $H^1_0$
\begin{equation}
\int_\Omega \left\langle \partial_t \bm{u} + A(\bm{u}) ,v\right\rangle = \int_\Omega \left\langle \bm{F} ,v\right\rangle , \quad \forall v \in H^1_0 \label{eq:weakform_problem} .
\end{equation}
Où $H^1_0$ est un espace de Sobolev :
\begin{equation}
H_0^1(\Omega) = \left \lbrace \bm{u} \in H^1 (\Omega),\gamma_0(\bm{u})=0 \right\rbrace
\end{equation}
où $H^s$, avec $s \in \mathbb{N}^*$, un espace de Sobolev définit par : 
\begin{equation}
H^s(\Omega) = \left \lbrace \bm{u} \in L^2 (\Omega),\quad \partial^\alpha \bm{u} \in L^2(\Omega), \vert \alpha \vert \leqslant s \right\rbrace
\end{equation}
et $\gamma_0$ est l'application \textit{trace}, $\gamma_0 : \bm{u} \rightarrow \bm{u}\vert_\Gamma$ qui à une fonction associe sa valeur du $\Gamma$. \\
Une approximation de la forme variationnelle \ref{eq:weakform_problem} est donnée en remplaçant $H^1_0 (\Omega)$ par le sous espace $V_h \subset H^1_0 (\Omega)$ des fonctions continues par morceaux. Le problème devient alors, trouver $\bm{u}_h \in V_h$
\begin{equation}
\int_\Omega \left\langle \partial_t \bm{u}_h + A(\bm{u}_h) ,v\right\rangle = \int_\Omega \left\langle \bm{F} ,v\right\rangle , \quad \forall v \in V_h \label{eq:weakform_bis_problem}
\end{equation}
En associant les équations  \ref{eq:problem_bdf} et \ref{eq:weakform_bis_problem}, on obtient le problème suivant, trouver $\bm{u}_h \in V_h$ 
\begin{equation}
\int_\Omega \left\langle \dfrac{\bm{u}^{n+1}_h}{\delta t} ,v\right\rangle + \int_\Omega \left\langle A(\bm{u}_h^{n+1}),v\right\rangle =  
\int_\Omega \left\langle \bm{F},v\right\rangle+\int_\Omega \left\langle f(\bm{u}_h^{n},\bm{u}_h^{n-1},\cdots,\bm{u}_h^{n-k}) ,v\right\rangle , \quad \forall v \in V_h \label{eq:weakform_problem_bdf}
\end{equation}

\section{Méthode de Newton}
L'une des techniques les plus efficaces pour résoudre les problèmes sont basées sur la méthode de Newton. \\
On considère le problème \ref{eq:problem} et sa forme variationnelle \ref{eq:weakform_problem_bdf}. Pour dériver la méthode de Newton, on écrit $\bm{u}$ comme
\begin{equation}
\bm{u} = \bm{u}^0 + \bm{\delta},
\end{equation}
où $\bm{u}^0$ est l'approximation courante et $\bm{\delta}$ est une petite correction. En subsistant dans l'équation \ref{eq:weakform_problem_bdf}, on obtient\footnote{Les indices $n+1$ et $h$ on été retiré par soucis de lecture, ainsi $\bm{u}^{n+1}_h\equiv\bm{u}$, de même pour $f\equiv  f(\bm{u}_h^{n},\bm{u}_h^{n-1},\cdots,\bm{u}_h^{n-k})$}
\begin{equation}
\int_\Omega \left\langle \dfrac{\bm{u}^0 + \bm{\delta}}{\delta t} , v\right\rangle +\int_\Omega \left\langle A(\bm{u}^0 + \bm{\delta} ) ,v\right\rangle = \int_\Omega \left\langle \bm{F},v\right\rangle + \int_\Omega \left\langle f,v\right\rangle \label{eq:weakform_to_DL}
\end{equation}
On s'intéresse maintenant au terme non linéaire $\left\langle A(\bm{u}^0 + \bm{\delta} ) ,v\right\rangle$ que l'on écrit au premier ordre grâce à un développement de Taylor
\begin{equation}
A(\bm{u}^0 + \bm{\delta}) \approx A(\bm{u}^0) + \nabla_{\bm{u}} A(\bm{u}^0) \bm{\delta}
\end{equation}
Le problème \ref{eq:weakform_to_DL} devient alors 
\begin{equation}
\int_\Omega \left\langle \dfrac{\bm{\delta}}{\delta t},v \right\rangle + \int_\Omega \left\langle \nabla_{\bm{u}} A(\bm{u}^0) \bm{\delta},v \right\rangle = \int_\Omega \left\langle\bm{F} ,v \right\rangle + \int_\Omega \left\langle f,v \right\rangle -\int_\Omega \left\langle \dfrac{\bm{u}^0}{\delta t},v \right\rangle -\int_\Omega \left\langle A(\bm{u}^0) ,v \right\rangle
\end{equation}
Soit $\left\lbrace \bm{\phi}_i\right\rbrace_{i=1}^N$ une base des fonctions test pour $V_h$, on peut alors écrire $\bm{\delta} = \sum_{i=1}^N d_i \bm{\phi}_i$, on peut alors alors écrire un problème linéaire en les $d_i$ : 
\begin{equation}
 \int_\Omega \left\langle \dfrac{ d_i \phi_i }{\delta t},v \right\rangle + \int_\Omega \left\langle d_i \phi_i \nabla_{\bm{u}}   A(\bm{u}^0) ,v \right\rangle = 
 \int_\Omega \left\langle\bm{F} + f,v \right\rangle -\int_\Omega \left\langle \dfrac{\bm{u}^0}{\delta t}+A(\bm{u}^0) ,v \right\rangle , \quad i=1,\ldots,N ,
\end{equation}
dont la forme matricielle s'écrit 
\begin{equation}
\bm{J} \bm{d} = \bm{r}
\end{equation}
où
\begin{enumerate}
 \item $\bm{J}$ est une matrice $N\times N$ appelée \textit{Jacobienne} 
 \begin{equation}
 J_{ij}= \int_\Omega \left\langle \dfrac{ \phi_j }{\delta t},\phi_i \right\rangle + \int_\Omega \left\langle \phi_j \nabla_{\bm{u}}   A(\bm{u}^0) ,\phi_i \right\rangle
 \end{equation}
 \item $\bm{r}$ est un vecteur de taille $N$, appelé \textit{Résidu} 
 \begin{equation}
  r_i=\int_\Omega \left\langle\bm{F} + f,\phi_i \right\rangle -\int_\Omega \left\langle \dfrac{\bm{u}^0}{\delta t}+A(\bm{u}^0) ,\phi_i \right\rangle
 \end{equation}
\end{enumerate}

Une meilleure approximation $\bm{u}$ par rapport à $\bm{u}^0$ peut être trouvée en ajoutant $\bm{\delta}$ à $\bm{u}^0$ puis à itérer, selon l'algorithme \ref{algo:methode_newton}. \\

\begin{algorithm}[H]
 \KwData{Choisir $\varepsilon$ petit et $\bm{u}^0=0$}
 \For{$k=0,1,2,\ldots$}{
 Assembler la matrice Jacobienne $\bm{J}$ et le vecteur résidu $\bm{r}$ \;
 Résoudre le système linéaire $ \bm{J} \bm{d} = \bm{r}$ \;
 Poser $\bm{u^{k+1}}=\bm{u}^k+\bm{\delta}^k$ \;
 \If{$ \Vert \bm{\delta}^k\Vert < \varepsilon $}{
    Stop \;
   }
 }
 \caption{Méthode de Newton} \label{algo:methode_newton}
\end{algorithm}


\section{Exemple}
On considère l'équation de Kuramoto-Sivashinsky (KS dans la suite)
\begin{equation}
\partial_t \bm{u} + \underbrace{\nu \nabla^4 \bm{u} + \nabla^2 \bm{u} + \bm{u} \nabla \bm{u}}_{A(\bm{u})} = 0 
\end{equation}
Dans ce cas, le gradient de $A(\bm{u})$ par rapport à $\bm{u}$ est assez simple
\begin{equation}
\nabla_{\bm{u}} A(\bm{u}) = \underbrace{\nu \nabla_{\bm{u}} (\nabla^4 \bm{u} )}_{=0} + \underbrace{\nabla_{\bm{u}} (\nabla^2 \bm{u} )}_{=0} + \underbrace{\nabla_{\bm{u}} (\bm{u} \nabla \bm{u} )}_{=\nabla \bm{u}} = \nabla \bm{u}
\end{equation}
La matrice Jacobien est donc simple à évaluer 
\begin{equation}
J_{ij} = \int_\Omega \left\langle \dfrac{ \phi_j }{\delta t},\phi_i \right\rangle + \int_\Omega \left\langle \phi_j \nabla \bm{u}^0  ,\phi_i \right\rangle
\end{equation}
Le résidu est plus compliqué à obtenir 
\begin{equation}
r_i = \int_\Omega \left\langle f,\phi_i \right\rangle + \int_\Omega \left\langle \dfrac{\bm{u}^0}{\delta t} + \nu \nabla^4 \bm{u}^0 + \nabla^2 \bm{u}^0 + \bm{u}^0 \nabla \bm{u}^0, \phi_i \right\rangle
\end{equation}
Chaque terme du résidu peut être développé selon la formule de Green ou un corollaire.
\begin{equation}
 \int_\Omega \left\langle \nu \nabla^4 \bm{u}^0 , \phi_i,\right\rangle=
 +\nu \int_\Omega \left\langle \nabla^2 \bm{u}^0 , \nabla^2 \phi_i \right\rangle
  + \nu\int_{\partial \Omega} \left\langle \nabla^2 \bm{u}^0 , \nabla \phi_i \right\rangle \bm{n}
  +\nu \int_{\partial \Omega} \left\langle \nabla^3 \bm{u}^0 , \phi_i \right\rangle \bm{n}
 \end{equation}
où $\bm{n}$ est le vecteur normale dirigé vers l'extérieur.
\begin{equation}
 \left\langle \nabla^2 \bm{u}^0 , \phi_i,\right\rangle= 
 - \int_\Omega \left\langle \nabla \bm{u}^0 , \nabla \phi_i,\right\rangle  
 + \int_{\partial \Omega} \left\langle \nabla\bm{u}^0 , \phi_i \right\rangle \bm{n}
 \end{equation}
 et
 \begin{equation}
\int_\Omega \left\langle \bm{u}^0 \nabla \bm{u}^0, \phi_i,\right\rangle= 
- \int_\Omega \left\langle \frac{1}{2}{\left(\bm{u}^0\right)}^2 , \nabla \phi_i,\right\rangle \bm{n} 
+ \int_{\partial \Omega} \left\langle \frac{1}{2}{\left(\bm{u}^0\right)}^2  , \nabla \phi_i \right\rangle \bm{n}
 \end{equation}


\end{document}